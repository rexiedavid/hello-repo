Video URL :https://www.youtube.com/watch?v=HVsySz-h9r4

USEFUL GIT / Windows Commands

git clone url destinationpath
    clone remote repo to local
    
git config -l 
    show list of config

git help [config]
    open browser for help page [config]
git [config] -help 

git init
    initialize a repository from existing code

rm -rf .git
    stop tracking project/ remove .git file

ls -la
    show current directory contents

git status 
    show current git local status, show untracked files

touch .gitignore
    create gitignore file - use to untrack a specific file 

    inside .gitignore file
        can use wildcard '*' to untrack multiple files

git add -A
    add ALL files to staging area
git add [filename]
    add specific file to staging area

git reset 
    remove fileds to staging area
git reset [filename]
    remove specific to staging area

git remote
    "remote" is the cloud repository

    -v
    list info about repository

git branch
    -a 
    list all branch on both local and remote repo

git diff 
    show changes made to the local code

git pull
    origin master
        pull changes from remote repository master branch

git push 
    origin master
        push changes from local repository master branch to remote repository master branch
    
    u origin [branchname] associate local branch to remote branch

git branch - show all branches
    [name] 
        create a branch
    create branch


---------------

Git Stages
    Working Directory > Staging Directory > .git directory (Repository)

#test update from SELF STUDY
